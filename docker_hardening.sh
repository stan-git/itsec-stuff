# Sign and verify images (is the image trustworthy and authentic?)
# Choose images with fewer OS libraries to minimize risk and attack surface
# Favor alpine based images over full-blown system OS images
# Least privileged policy
# User and Groups only with minimal permission

# Scan for known vulnerabilities with tools such as Synk
synk test --docker node:10 --file=path/to/Dockerfile

# Use Docker-bench-security
https://github.com/docker/docker-bench-security

# Synk monitor and alert newly disclode vulnerabilities in a Docker image
synk monitor --docker node:10

# Cgroups
# AppArmor/SELinux (via runc)
# seccomp
# userns

# Docker content trust
sudo export DOCKER_CONTENT_TRUST=1

# Set resource limits for containers
# Limit container to 2 CPUs add this option to the run command
--cpus=2

# Limit to 1GB of memory
--memory="1000M"

# Display images that are official build with for example "WordPress" in their name
docker search --filter "is-official=true" WordPress

# Docker Bench Security

# Use multi-stage build to create small and clean images
# Use linter (static code analyzer) like hadolint linter
# for detecting issues in a dockerfile

# Use a SECURITY.TXT policy file to implement a security disclosure policy
# provide this information in your image label

# Use COPY instead of ADD if not specifically required
# ADD is vulnerable for MITM

# Use Docker secrets to mount sensitive files without caching them

# Use a .dockerignore file to avoid a hazardous COPY Instruction,
# which my pull sensitive files

# Use fixed tags for immutability
