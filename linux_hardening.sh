###
#
#
# LINUX Hardening
# 1. create an account for each user who should access the system
# 2. enforce to use strong passwords 
# 3. use sudo to delegate admin access 
#
###


# Password Protect BIOS and Bootloader
grub-mkpasswd-pbkdf2

# Create alias for update
alias update='sudo apt-get update -y && sudo apt-get full-upgrade -y --auto-remove && sudo apt-get dist-upgrade -y && sudo apt-get clean -y && sudo apt-get autoclean -y'

# disable unnecessary services (FTP, Telnet, Rlogin/Rsh, etc...)
# list all services in ubuntu
systemctl -at service

# stop service
sudo systemctl stop [service]

# disable service
sudo systemctl disable [service]
# alternative
sudo update-rc.d –f [service] remove

# kill process
sudo kill -9 [process_id]

# uninstall unnecessary software (tools for help: UnusedPkg diagnostics, Deborphan)
# list all installed Packages
dpkg --list
# alternative
apt list --installed

# remove Libs and Packages that were installed automatically
apt-get autoremove

# uninstall package
sudo apt remove [package]  

# install antivirus
sudo apt install clamav # (clamscan)

# enforce strong password management (install PAM)
# remember passwords in PAM. Restrict User from using previous passwords
# -a all unpacked, but unconfigured packages are configured
sudo dpkg --configure -a
sudo apt --fix-broken install
sudo apt-get -y install libpam-pwquality cracklib-runtime 
# edit /etc/pam.d/common-password
# pam_pwquality.so retry=3 minlen=9 difok=4 lcredit=-2 ucredit=-2 dcredit=-1 ocredit=-1 reject_username enforce_for_root
# retry: No. of consecutive times a user can enter an incorrect password.
# minlen: Minimum length of password
# difok: No. of character that can be similar to the old password
# lcredit: Min No. of lowercase letters
# ucredit: Min No. of uppercase letters
# dcredit: Min No. of digits
# ocredit: Min No. of symbols
# reject_username: Rejects the password containing the user name
# enforce_for_root: Also enforce the policy for the root user

# Set password expiry
# /etc/login.defs

# Restriction Logon Hours
# edit /etc/security/time.conf
# Login;*;!USER;MoTuWeThFr0800-2000

# Restriction Internet Access between 0800 and 2000
# edit /etc/security/time.conf
# http;*;!USER'MoTuWeThFr0800-2000

# ensure no accounts have empty passwords
# list all the accounts with empty passwords
awk -F: '($2 == "") {print}' /etc/shadow

# disable unnecessary accounts
# view users who have been inactive from past 90 days
lastlog -b 90 | tail -n+2 | grep -v 'Never logged in'

# disable user
usermod -L [username]

# secure shared memory
# /run/shm should mount in read-only without permissions to execute programs
# edit /etc/fstab and include following line to set /run/shm to read-only
tmpfs /run/shm tmpfs defaults,noexec,nosuid 0 0

# delete x window (if not needed)
# if you want to disable
# edit /etc/inittab and set run level to 3
vim /etc/inittab
# find line id:5:initdefault:
# replace with id:3:initdefault:l
# if you want to remove x window
yum groupremove "X Window System"

# create seperate disk partitions for /usr, /home, /var, /var/tmp, /tmp
# and for Apache and ftp server roots
# edit and update following configuration settings in /etc/fstab
# noexec: do not set execution of any binaries on this partition
# nodev: do not allow character or special devices on this partition
# nosuid: do not set SUID/SGID access on this partition (prevent the setuid bit)

# enable disk quotas
# to limit the number of files a user can create on the system
sudo edquota [username]
# check user
sudo quota -vs [username]

# list all files with SUID set
find / -perm /4000
# list all files with SGID set
find / -perm /2000
# remove setuid bit
chmod a-s /usr/bin/chfn

# view world-writable file without sticky bit
find /home/alice -xdev -type d \( -perm -0002 -a ! -perm -1000 \) -print

# view noowner files
find /home/alice -xdev \( -nouser -nogroup \) -print

# block usb-storage module (disable usb storage)
sudo mv usb-storage.ko usb-storage.ko.blacklist

# monitor open ports
netstat -tulpn
ss -tulpn

# disable IPv6 if not needed
# open /etc/sysctl.conf
# add at the bottom of the file
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1

# create group for ssh user
# diable ssh root login
# /etc/ssh/sshd_config
# DenyUsers [user]
# DenyGroups [Group]
# and change loglevel to VERBOSE in that file

# setup chroot sftp
# https://www.thegeekstuff.com/2012/03/chroot-sftp-setup/

# use Lynis for health scan
# https://cisofy.com/lynis/#download
# git clone https://github.com/CISOfy/lynis.git

# Turn on AppArmor, SELinux
# OpenSCAP (apt-get install libopenscap8, dnf install openscap-scanner, yum install openscap-scanner)
# JShielder, Bastille Linux, nixarmor, bane, Comodo Antivirus, Grsecurity
# Firejail (sandboxing)
# dm-crypt (Disk Encryption)
# GnuPrivacy Guard (File Encryption)
# Disk Utility (Removable Media Encryption)
