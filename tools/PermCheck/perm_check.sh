#!/usr/bin/env bash

###################################################################
#Script Name	: perm_check.sh                                                                                          
#Description	: This tool is for checking the permissions of sensitive files and directories                                                                                
#Author       	: S.Janik                                           
###################################################################

PATH_FILE="./paths_to_check.txt"
RED='\033[0;31m'
NC='\033[0m' 			# No Color

while read line;
do
        FPERM=${line: -3}
        FNAME=${line::-4}
        REAL_PERM=$(stat -c '%a' $FNAME 2>/dev/null)

	if [[ ($REAL_PERM -ne $FPERM) && ($? -eq 0) ]];
	then
		echo -e "Filename:\t$FNAME\tPerm:\t$REAL_PERM -> should ${RED}$FPERM${NC}"
        fi
done < $PATH_FILE 
