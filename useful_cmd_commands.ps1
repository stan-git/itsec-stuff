Gpresult starts the Operating System Group Policy Result tool

ipconfig /flushdns flushes the DNS resolver cache; helpful when troubleshooting DNS name resolution problems

nbtstat -a < MachineName> obtains info from WINS or LMHOST (discovers who is logged on)

nbtstst -A obtains info from WINS or LMHOST (discovers who is logged on)

nbtstat –R purges and reloads the remote cache name table

nbtstat –n lists local NetBIOS names

nbtstat –r useful for detecting errors when browsing WINS or NetBIOS

netstat –ab the b switch links each used port with its application

netstat –an shows open ports

netstat -an 1 | find "15868" locates only lines with the number 15868 and redisplays every one second

netstat -an | find "LISTENING" shows open ports with LISTENING status

net use retrieves a list of network connections

net user shows the user account for the computer

net user /domain displays user accounts for the domain

net user /domain shows account details for specific user

net group /domain shows group accounts for the domain

net view displays domains in the network

net view /domain specifies computers available in a specific domain

net view /domain: | more shows user accounts from specific domain

net view /cache shows workstation names

ping -a (IP) resolves IP to hostname

ping -t (IP) pings host until stopped Pathping displays the route and ping information when performing queries such as –n and –h options representing hostnames and maximum hops, respectively

set U shows which user is logged on

set L shows the logon server

telnet (IP) (port) confirms whether the port is open.
