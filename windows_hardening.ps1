###
#
#
# WINDOWS 10 Hardening
#
#
###

# Disabled Guest-Account
net user guest | findstr /C:"active"
net user guest /active:No

# Disable unnecessary Local Administrator Accounts
net user Administrator | findstr /C:"active"
net user Administrator /active:No

# Enable complex passwords
##### AD = Active Directory commands 
set-ADDefaultDomainPasswordPolicy -identity cnd.com -ComplexityEnabled $True
get-ADDefaultDomainPasswordPolicy
 
# Set PasswordAge
set-ADDefaultDomainPasswordPolicy -identity cnd.com -MaxPasswordAge 10.00:00:00

# View default password policy
get-ADDefaultDomainPasswordPolicy

# Set min length of password
set-ADDefaultDomainPasswordPolicy -identity cnd.com -MinPasswordLength 11
get-ADDefaultDomainPasswordPolicy

# Enable Automatic Updates
sc.exe config wuauserv start= auto

# Get Access information
Get-Acl C:\Demo

# Turn on User Account Control
New-Itemproperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -PropertyType DWord -Value 1 -Force

# View Services and state
Get-WmiObject -Class WIN32_Service -ComputerName WIN-TLT15MCTC46 | Select __SERVER, Name, state, startName

# Disable Service
Set-Service svsvc -StartupType Disabled

# Enable Windows Defender Firewall
Set-NetfirewallProfile -Profile Domain, Public, Private -Enabled true

# Viewing Firewall Status
New-NetFirewallRule -RemoteAddress 10.10.10.12/24 -DisplayName "Local subnet" -Direction inbound -Profile Any -Action Allow

# Getting Firewall Rules
Get-NetFirewallRule -Name *HTTP* | Select Name, Enabled, Direction, Action, PrimaryStatus

# Force User to change password at next login
Set-ADUser -identity Alice -ChangePasswordAtLogon $true

# View Accounts having password set to never expire
Get-ADUser -filter * -properties Name, PasswordNeverExpires | where {$_.passwordNeverExpires -eq "true"} | Select-Object DistingushedName, Name, Enabled

Get-ADUser -filter * -properties Name, PasswordNeverExpires

# Disable User-Account
Disable-ADAccount -Identity Alice

# List all disabled account
Search-ADAccount -AccountDisabled | Select name

# Serach for locked out user
Search-ADAccount -LockedOut

# and unlock
Search-ADAccount -LockedOut | UnLock-ADAccount

# View users login details
Get-ADDomainController -Filter *

# Disable inactive accounts
Search-ADAccount -UsersOnly -AccountInactive -Datetime '6/3/2020' | Disable-ADAccount
# with Timespan
$timespan = New-Timespan -Days 90
Search-ADAccount -UsersOnly -AccountInactive -TimeSpan $timespan | Disable-ADAccount

# Disable PS 2.0
Disable-WindowsoptionalFeature -Online -FeatureName MicrosoftWindowsPowerShellV2Root

# Disable SMB 1.0
Set-SmbServerConfiguration -EnableSMB1Protocol $false

# Enable SMB Encryption
# Individual File
Set-SmbShare -Name <sharename> -EncrptData $true
# Entire file server
Set-SmbServerConfiguration - EncryptData $true
# Create new SMB file share with SMB encryption enabled
New-SmbShare -Name <sharename> -Path <pathname> -EncryptData $true

# View Registry Key Data
Get-PSDrive -PSProvider Registry | Select-Object -Property Name,Root

# Navigate to Registry Path and get Key Values
Set-Location HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion
Get-ChildItem

# Configure PUA-Protection
Set-MpPreference -PUAProtection 1

# Turn on LAPS
# DisallowRun Registry
# Turn on Applocker, AirLock Digital, Ivanti Application Control, Gatekeeper, PowerBroker ...
# Windows Features -> Sandbox; Windows Defender Application Guard
# Use Microsoft Security Compliance Toolkit
